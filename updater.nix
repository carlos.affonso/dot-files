{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.dotfiles.lindenk;
in
{
  options.dotfiles.lindenk = {
    update.enable = mkEnableOption "Enable service to auto update lindenk's dotfiles";
    flavor = mkOption {
      type = types.enum [ "server" "desktop" "minimal" ];
      default = "minimal";
      description = "Profile to use";
    };
    dotfileDirectory = mkOption {
      type = types.str;
      default = "/home/lindenk/.config/dotfiles";
      description = "Directory to store dotfiles";
    };
    branch = mkOption {
      type = types.str;
      default = "master";
      description = "Git branch to use";
    };
  };

  config = mkIf cfg.update.enable {
    systemd.services."dotfiles-lindenk" = {
      serviceConfig = {
        Type = "oneshot";
        User = "lindenk";

        # systemd can't create it's own working directores
        #WorkingDirectory = cfg.dotfileDirectory;
      };
      script = ''
        mkdir -p "${cfg.dotfileDirectory}"
        cd "${cfg.dotfileDirectory}"

        # clone if it doesn't exist yet (fail if dir is not empty)
        ${pkgs.git}/bin/git rev-parse 2> /dev/null \
          || ${pkgs.git}/bin/git clone https://gitlab.com/lindenk/dotfiles-nix.git . \
          || exit 1

        # use the given branch
        ${pkgs.git}/bin/git checkout "${cfg.branch}"

        # don't update if dirty
        ${pkgs.git}/bin/git update-index --refresh 
        ${pkgs.git}/bin/git diff-index --quiet HEAD -- || exit 1

        # pull
        ${pkgs.git}/bin/git pull origin "${cfg.branch}"

        # switch
        PATH=/run/current-system/sw/bin:
        "${pkgs.home-manager}/bin/home-manager" switch --flake ".#lindenk-${cfg.flavor}" --extra-experimental-features 'nix-command flakes'
      '';
    };

    systemd.timers."dotfiles-lindenk" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnCalendar = "daily";
        Persistent = true;
        Unit = "dotfiles-lindenk.service";
      };
    };
  };
}
