{ pkgs, config, lib, ... }:
with lib;
let cfg = config.modules.qutebrowser;
in
{
  options.modules.qutebrowser.enable = mkEnableOption "Webkit hotkey-driven Web Browser";

  config = mkIf cfg.enable {
    programs.qutebrowser = {
      enable = true;
      settings = {
        scrolling.smooth = true;
        downloads.location.directory = "~/downloads";
        editor.command = ["${pkgs.vscode}/bin/code" "-n" "{}"];
        hints.chars = "asdfghjkl;";
        auto_save.session = true;
        content.cookies.accept = "all";
        input.insert_mode.leave_on_load = false;
        input.insert_mode.auto_leave = false;
      };

      enableDefaultBindings = false;
      keyBindings = {
        normal = {
          # Reload
          "<escape>" = "clear-keychain ;; search ;; fullscreen --leave";
          "r" = "reload";
          "<f5>" = "reload";
          "R" = "reload -f";
          "<ctrl-f5>" = "reload -f";

          # Search page
          ";" = "cmd-set-text /";
          ":" = "cmd-set-text :";
          "." = "search-next";
          "," = "search-prev";

          # New tab
          "a" = "cmd-set-text -s :open";
          "A" = "cmd-set-text -s :open -t";
          "<ctrl-a>" = "cmd-set-text -s :open -w";
          "<ctrl-A>" = "cmd-set-text -s :open -b";

          # Tab manip
          "x" = "tab-close";
          "X" = "undo";
          "d" = "tab-clone";
          "<ctrl-d>" = "tab-clone -w";

          # Tab nav
          "<ctrl-j>" = "tab-prev";
          "<ctrl-l>" = "tab-next";
          "h" = "tab-focus 1";
          "H" = "tab-focus -1";

          "<ctrl-shift-j>" = "tab-move -";
          "<ctrl-shift-l>" = "tab-move +";

          "<ctrl-1>" = "tab-focus 1";
          "<ctrl-2>" = "tab-focus 2";
          "<ctrl-3>" = "tab-focus 3";
          "<ctrl-4>" = "tab-focus 4";
          "<ctrl-5>" = "tab-focus 5";
          "<ctrl-6>" = "tab-focus 6";
          "<ctrl-7>" = "tab-focus 7";
          "<ctrl-8>" = "tab-focus 8";
          "<ctrl-9>" = "tab-focus 9";

          # Back/Forward
          "u" = "back";
          "<back>" = "back";
          "U" = "back -t";
          "o" = "forward";
          "<forward>" = "forward";
          "O" = "forward -t";

          # Links
          "s" = "hint all normal";
          "S" = "hint all tab-fg";
          "<ctrl-s>" = "hint all --rapid tab-bg";
          "<ctrl-S>" = "hint all window";

          "c" = "hint all yank";
          "C" = "yank pretty-url";
          "v" = "open {clipboard}";
          "V" = "open -t {clipboard}";

          "D" = "hint all download";
          "<ctrl-escape>" = "download-remove -a";

          "e" = "mode-enter insert";
          "E" = "hint inputs";

          # Scrolling
          "j" = "scroll left";
          "k" = "scroll down";
          "i" = "scroll up";
          "l" = "scroll right";

          "J" = "scroll-page -0.2 0";
          "K" = "scroll-page 0 0.5";
          "I" = "scroll-page 0 -0.5";
          "L" = "scroll-page 0.2 0";

          "<ctrl-i>" = "scroll-to-perc 0";
          "<ctrl-k>" = "scroll-to-perc";

          # Zoom
          "Z" = "zoom-out";
          "z" = "zoom-in";
          "=" = "zoom";

          # Misc
          "<return>" = "follow-selected";
          "<enter>" = "follow-selected";
          "<shift-return>" = "follow-selected -t";
          "<shift-enter>" = "follow-selected -t";

          "P" = "tab-pin";
        };
        insert = {
          "<ctrl-e>" = "open-editor";
          "<escape>" = "mode-leave";
        };
        hint = {
          "<return>" = "follow-hint";
          "<shift-return>" = "follow-hint";
          "<enter>" = "follow-hint";
          "<shift-enter>" = "follow-hint";

          "<escape>" = "mode-leave";
        };
        command = {
          "<shift-tab>" = "completion-item-focus prev";
          "<tab>" = "completion-item-focus next";

          "<up>" = "command-history-prev";
          "<down>" = "command-history-next";

          "<return>" = "command-accept";
          "<shift-return>" = "command-accept";
          "<enter>" = "command-accept";
          "<shift-enter>" = "command-accept";

          "<ctrl-c>" = "rl-yank";

          "<escape>" = "mode-leave";
        };
        prompt = {
          "<return>" = "prompt-accept";
          "<shift-return>" = "prompt-accept";
          "<enter>" = "prompt-accept";
          "<shift-enter>" = "prompt-accept";

          "<shift-tab>" = "prompt-item-focus prev";
          "<up>" = "prompt-item-focus prev";

          "<tab>" = "prompt-item-focus next";
          "<down>" = "prompt-item-focus next";

          "<ctrl-c>" = "rl-yank";

          "<escape>" = "mode-leave";
        };
        yesno = {
          "<escape>" = "mode-leave";
          "<return>" = "prompt-accept";
          "n" = "prompt-accept no";
          "y" = "prompt-accept yes";
        };
      };
    };
  };
}
