{ pkgs, config, lib, ... }:
with lib;
let cfg = config.modules.mpd;
in
{
  options.modules.mpd.enable = mkEnableOption "Music player daemon";

  config = mkIf cfg.enable {
    services.mpd = {
      enable = true;

      musicDirectory = "${home.homeDirectory}/music";

      extraConfig = ''
        audio_output {
          type "pipewire"
          name "Mpd PipeWire Output"
        }
      '';
    };

    services.mpdris2 = {
      enable = true;
    };

    services.mpd-discord-rpc = {
      enable = true;
    };

    # todo listenbrainz and beets
  };
}
