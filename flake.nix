{
  description = "@Lindenk's dotfiles using nix and home-manager";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    home-manager = {
      url = "github:nix-community/home-manager/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    agenix.url = "github:ryantm/agenix";
  };

  outputs = { nixpkgs, nixpkgs-unstable, home-manager, flake-utils, agenix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        pkgs-unstable = import nixpkgs-unstable { inherit system; config.allowUnfree = true; };
        scripts = import ./scripts.nix { inherit pkgs; };

        mkHome = { modules }:
          home-manager.lib.homeManagerConfiguration {
            inherit pkgs;

            modules = [
              ./home.nix
              ./secrets/default.nix
              agenix.homeManagerModules.age
              {
                home.packages = [
                  agenix.packages.${system}.default
                ];
              }
            ] ++ modules;
            extraSpecialArgs = { inherit pkgs-unstable nixpkgs scripts; };
          };
      in
      {
        packages.homeConfigurations = {
          jmuller = mkHome { modules = [ ]; };
        };

        updater = ./updater.nix;
      }
    );
}
