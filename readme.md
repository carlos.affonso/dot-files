# Installation

Update channels if they aren't up to date:
`nix-channel --update; nix-env -iA nixpkgs.nix`

First time run (without home-manager installed):
`nix shell 'nixpkgs#home-manager'`

Afterwards, use this to install:
`home-manager switch --flake .`


