{ lib, scripts, pkgs, pkgs-unstable, ... }:
{
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowUnfreePredicate = _: true;

  imports = [
    ./modules/gpg.nix
    ./modules/hyprland.nix
    ./modules/waybar.nix
    ./modules/alacritty.nix
    ./modules/qutebrowser.nix
  ];

  # env vars
  home.sessionVariables = {
    NIXOS_OZONE_WL = "1";
  };

  home.packages = lib.mkMerge [
    (with pkgs; [
      # GUI programs
      firefox
      chromium
      imv
      mpv
      grimblast

      # other
      ffmpeg
      amdgpu_top
      obs-studio

      (discord.override {
        withOpenASAR = true;
        withVencord = true;
      })

      # de stuff
      rofi
      wofi
      wl-clipboard

      # games
      runelite
    ])
    (with pkgs-unstable; [
      logseq
      nil
    ])
  ];

  xdg = {
    desktopEntries = {
      qutesession = {
        name = "Qutebrowser Session";
        genericName = "Web Browser";
        exec = lib.getExe scripts.qutebrowser-current-session;
        terminal = false;
        categories = [ "Application" "Network" "WebBrowser" ];
        mimeType = [ "text/html" ];
      };
    };
    mimeApps = {
      enable = true;
      
      defaultApplications = {
        "text/html" = "qutesession.desktop";ttpie
        "x-scheme-handler/http" = "qutesession.desktop";
        "x-scheme-handler/https" = "qutesession.desktop";
        "x-scheme-handler/about" = "qutesession.desktop";
        "x-scheme-handler/unknown" = "qutesession.desktop";
      };
    };
  };

  programs.vscode = {
    enable = true;
    package = pkgs-unstable.vscode.fhsWithPackages 
      (ps: with ps; [
        rustup
        zlib
        openssl
        openssl.dev
        pkg-config
        gcc
      ]
    );
    extensions = with pkgs-unstable.vscode-extensions; [
      arrterian.nix-env-selector
      
      jnoortheen.nix-ide
      rust-lang.rust-analyzer
      tamasfe.even-better-toml
      ms-python.python
      ms-python.vscode-pylance
      ms-python.isort

      ms-vsliveshare.vsliveshare
    ];
    userSettings = {
      # tab size
      "editor.tabSize" = 2;
      "prettier.tabWidth" = 2;
      "python.formatting.autopep8Args" = [
        "--indent-size=2"
        "--ignore E121"
      ];

      # disable the excessive header
      "window.titleBarStyle" = "native";
      "window.customTitleBarVisibility" = "never";
      "editor.titleBar.enabled" = false;
      "window.commandCenter" = false;
      "workbench.layoutControl.enabled" = false;
      "window.menuBarVisibility" = "hidden";

      # misc editor
      "editor.fontFamily" = "monospace,\"Hack Nerd Font\"";
      "editor.wordWrap" = "on";
      "editor.formatOnPaste" = true;
      "editor.formatOnSave" = true;
      "editor.codeActionsOnSave" = {
        "source.fixAll" = "never";
        "source.fixAll.eslint" = "explicit";
      };

      # disable updates
      "update.mode" = "none";
      "extensions.autoUpdate" = false;

      # misc
      "security.workspace.trust.enabled" = false;

      # extensions
      ## liveshare
      "liveshare.accessibility.voiceEnabled" = false;

      ## nix-ide
      "nix.enableLanguageServer" = true;
      "nix.serverPath" = "${lib.getExe pkgs-unstable.nil}";

      ## nix env selector
      "nixEnvSelector.suggestion" = false;
    };
  };

  dotfiles.age.home-assistant.enable = true;

  services.mako = {
    enable = true;
  };

  #services.cliphist.enable = true; # currently saves passwords. Wait for a fix

  modules.alacritty.enable = true;
  modules.gpg.enable = true;
  modules.hyprland = {
    enable = true;
    monitors = [
      "monitor=DP-1,1920x1080@144,auto,1"
      "monitor=DP-3,1920x1080@144,auto,1"
      "monitor=HDMI-A-1,1920x1080@60,auto,1"
    ];
  };
  modules.waybar.enable = true;
  modules.qutebrowser.enable = true;
}
