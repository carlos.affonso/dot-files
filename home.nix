{ lib, pkgs, pkgs-unstable, nixpkgs, ... }:

rec {
  home = {
    username = "jmuller";
    homeDirectory = "/home/jmuller";
    stateVersion = "23.05";
  };

  imports = [
    ./modules/zsh.nix
  ];

  home.packages = lib.mkMerge [
    (with pkgs; [
      # Basic fs navigation and utilities
      lsd
      bat
      ripgrep
      carapace
      erdtree
      tealdeer

      # General utils
      git
      pass
      dtrx
      python3
      jq

      # Networking
      #nixops
      httpie
      wormhole-rs
      dig
      dogdns
      aria2

      # Theming a UI
      #pywal
    ])
    (with pkgs-unstable; [
      btop
    ])
  ];

  # Nix
  nix = {
    enable = true;
    package = pkgs.nix;
    settings = {
      experimental-features = "nix-command flakes";
      max-jobs = "auto";
    };
  };

  # Program configuration
  programs.home-manager = {
    enable = true;
    #  path = "$HOME/new-dotfiles";
  };

  home.sessionVariables = {
    SHELL = "${pkgs.zsh}/bin/zsh";
    NIX_PATH = "nixpkgs=${nixpkgs.outPath}";
    #TERM = "alacritty";
    EDITOR = "nano";
  };
  xdg = {
    configHome = "${home.homeDirectory}/.config/";
    dataHome = "${home.homeDirectory}/.local/share/";
  };

  modules.zsh.enable = true;

  programs.git = {
    enable = true;
    userName = "Joey Muller";
    userEmail = "joey.muller@staticsoftware.net";

    aliases = {
      c = "commit";
      cm = "commit -m";
      a = "add -A";
      s = "status";
      f = "fetch";
      p = "pull";
      co = "checkout";
    };

    extraConfig = {
      pull.rebase = true;
      diff.tool = "code";
      merge.tool = "code";
    };
  };
}
